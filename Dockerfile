FROM node:13.2.0-alpine3.10

LABEL version="1.0.0" description="Projeto VivaEScola" maintainer="Well Tecnologia <welbermsantana@gmail.com>"

WORKDIR /app

COPY ./api /app/api
COPY ./web /app/web

RUN touch /app/api/.env
RUN echo DATABASE_CONNECTION_STRING=${DB_URL} >> /app/api/.env
RUN echo SECRET=${SECRET_KEY} >> /app/api/.env
RUN echo PORT=${PORT_API} >> /app/api/.env

COPY ./api/package.json /app/web/package.json

RUN cd /app/api/; npm install

RUN touch /app/web/.env; echo NODE_ENV=production >> .env; npm install

COPY ./web/package.json /app/web/package.json

RUN cd /app/web; npm install

COPY ./start.sh /app/start.sh

RUN chmod 777 /app/start.sh

EXPOSE 8080
EXPOSE 3000

CMD ["./start.sh"]