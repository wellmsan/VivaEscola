import axios from 'axios'

const standard = axios.create({
    baseURL: 'https://vivaescola.well.eti.br:3000',
    headers: { 'X-Custom-Header': 'foobar' }
})

// standard.interceptors.response.use(..., ...)

export default standard