#!/bin/bash

cd /app/api
touch .env
echo NODE_ENV=production >> .env
echo DATABASE_CONNECTION_STRING=${DB_URL} >> .env
echo SECRET=${SECRET_KEY} >> .env
echo PORT=${PORT_API} >> .env

# Inicia API
cd /app/api
npm run start &

cd /app/web
touch .env
echo NODE_ENV=production >> .env

# Inicia Módulo Web
cd /app/web
npm run build
npm install -g serve 
serve -s dist -p 8080